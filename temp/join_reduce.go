package main;

import (
	"bufio"
	"fmt"
	"os"
//	"strconv"
	"strings"
)

var datasetRecords map[string][]string= make(map[string][]string)
var outputFilePrefix string = "juice"

func processLine(line string){
	tokens := strings.Split(line, "\t") // should be two of them
	datasetRecords[tokens[0]] = append(datasetRecords[tokens[0]], tokens[1])
}

func main(){
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) < 1{
		fmt.Printf("Usage: ./juice_exe <juice_number> <reduced join file to actually perform the join on>")
		return
	}
	if len(argsWithoutProg) == 1 {
		return
	}
	fnames := argsWithoutProg[1:]
	for _, fname := range(fnames) {
		file, err := os.Open(fname)
		if err != nil {
			fmt.Println(err)
			continue;
		}
		fileScanner := bufio.NewScanner(file)
		fileScanner.Split(bufio.ScanLines)
		for fileScanner.Scan() {
			processLine(fileScanner.Text());
		}
		file.Close()
	}
	//done getting all the lines, time to process the dataset records
	dsetnames := make([]string, 0)
	for dsetname, _ := range(datasetRecords){
		dsetnames = append(dsetnames, dsetname)
	}
	fmt.Printf("Dataset names: %v\n", dsetnames);
	d0 := dsetnames[0]
	d1 := dsetnames[1]
	// done processing, now time to output
	outputFname := outputFilePrefix + "_" + argsWithoutProg[0] + ".out"
	outputFile, err := os.Create(outputFname)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer outputFile.Close()
	for _, record1 := range(datasetRecords[d0]){
		for _, record2 := range(datasetRecords[d1]){
				outputLine := record1 + ";" + record2;
				outputFile.Write([]byte(outputLine + "\n"))
		}
	}
}
