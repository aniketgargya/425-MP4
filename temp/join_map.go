package main;

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var setOfKeys map[string]int = make(map[string]int);
var outputFilePrefix string = "maple"
var cliArgs []string


func processLine(line string, mapleNumber string){
    tokens := strings.Split(line, ",")
	desiredIdx, _ := strconv.ParseInt(cliArgs[0], 10, 64)
	desiredComValue := cliArgs[1]
    if tokens[desiredIdx] == desiredComValue {
        setOfKeys["_"] = 1
        file, err := os.OpenFile((outputFilePrefix + "_" + "_" + ".txt"), os.O_APPEND | os.O_WRONLY | os.O_CREATE, 0600)
        if err != nil {
            panic(err)
        }
        defer file.Close()
        log.Printf("Successfully opened %s\n", (outputFilePrefix + "_" + "_" + ".txt"))

        output := cliArgs[2] + "\t" + line // <dataset_name> \t <line in question>
        file.Write([]byte(output + "\n"))
        log.Printf("Wrote %s to the file\n", output);
        fmt.Printf("%s\n", (output));
    } else {
        // fmt.Printf("tokens[%d]: %v\n", desiredIdx, tokens[idx])
        // fmt.Printf("tokens: %v\n", tokens)
    }
}

func main(){
		argsWithoutProg := os.Args[1:]
		if len(argsWithoutProg) < 6 {
			fmt.Println("Usage: <exec> <myMapleNumber> <totalMaples> <inputFile> <index of each line's split aka the field position in line as an int> <desired value of the field to join against> <dataset name (can be a number, just make it distinct between datasets>");
			return;
		}
        fmt.Printf("os.Args: %v\n", os.Args)
        fmt.Printf("argsWithoutProg: %v\n", argsWithoutProg)
		cliArgs = argsWithoutProg[3:] // the cli args should be <idx_of_split_line(used in line 21)> <desired_value> <dataset_name (can be just a number)> 
		logfile, err := os.OpenFile("testlogfile" + argsWithoutProg[0] + ".log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
		if err != nil {
			fmt.Println(err);
		}
		log.SetOutput(logfile)
        log.Printf("os.Args: %v\n", os.Args)
        log.Printf("argsWithoutProg: %v\n", argsWithoutProg)
        log.Printf("cliArgs: %v\n", cliArgs)
		defer logfile.Close()
		fmt.Println(argsWithoutProg);
		myMapleNumber, err := strconv.ParseInt(argsWithoutProg[0], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		outputFilePrefix = outputFilePrefix + strconv.Itoa(int(myMapleNumber))
		totalMaples, err := strconv.ParseInt(argsWithoutProg[1], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		inputFile := argsWithoutProg[2]
		keyFile := "keys" + strconv.Itoa(int(myMapleNumber)) + ".txt"
		fmt.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s\n", myMapleNumber, totalMaples, inputFile, keyFile);
		log.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s\n", myMapleNumber, totalMaples, inputFile, keyFile);
		file, err := os.Open(inputFile)
		if err != nil {
			fmt.Println(err)
		}
		idx := int64(0)
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			if idx % totalMaples == myMapleNumber {
				fmt.Printf("Gonna process this line %d now: %s\n", idx, line);
				log.Printf("Gonna process this line %d now: %s\n", idx, line);
				processLine(line, argsWithoutProg[0])
			}
			idx++;
		}
		for key, _ := range(setOfKeys){
			file, err := os.OpenFile(keyFile, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
			defer file.Close()
			if err != nil {
				panic(err)
			}
			log.Printf("Opened the file %s\n", keyFile);
			file.Write([]byte(key + "\n"));
			log.Printf("Wrote %s to %s\n", key, keyFile);
		}
		fmt.Println("Done\n");
}
