package main;

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var setOfKeys map[string]int = make(map[string]int);
var outputFilePrefix string = "maple"

func processLine(line string, mapleNumber string){
		tokens := strings.Split(line, ",")
		for _, token := range(tokens){
			setOfKeys[token] = 1
			file, err := os.OpenFile((outputFilePrefix + "_" + token + ".txt"), os.O_APPEND | os.O_WRONLY | os.O_CREATE, 0600)
			if err != nil {
				panic(err)
			}
			defer file.Close
			output := tokens[0] + ":" + token + "," + strconv.Itoa(1) + ";" + mapleNumber
			file.Write([]byte(output + "\n"))
		}
}

func main(){
		argsWithoutProg := os.Args[1:]
		if len(argsWithoutProg) != 3 {
			fmt.Println("Usage: <exec> <myMapleNumber> <totalMaples> <inputFile>");
			return;
		}
		fmt.Println(argsWithoutProg);
		myMapleNumber, err := strconv.ParseInt(argsWithoutProg[0], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		outputFilePrefix = outputFilePrefix + strconv.Itoa(int(myMapleNumber))
		totalMaples, err := strconv.ParseInt(argsWithoutProg[1], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		inputFile := argsWithoutProg[2]
		keyFile := "keys" + strconv.Itoa(int(myMapleNumber)) + ".txt"
		fmt.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s\n", myMapleNumber, totalMaples, inputFile, keyFile);
		fmt.Printf("New bitch\n");
		file, err := os.Open(inputFile)
		if err != nil {
			fmt.Println(err)
		}
		idx := int64(0)
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			if idx % totalMaples == myMapleNumber {
				fmt.Println("Gonna process this line %d now: %s\n", idx, line);
				processLine(line, argsWithoutProg[0])
			}
			idx++;
		}
		for key, _ := range(setOfKeys){
			file, err := os.OpenFile(keyFile, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
			defer file.Close()
			if err != nil {
				panic(err)
			}
			file.Write([]byte(key + "\n"));
		}
		fmt.Println("Done\n");
}
