import os
import sys
import textwrap
import re

regular_expression = sys.argv[1]
outputFile = sys.argv[2]
linefeed = "\\n"
exp = f"""{regular_expression}"""
exp = re.escape(exp)
print(exp)

code_snippet = textwrap.dedent(
f'''package main;
import (
    "bufio"
    "fmt"
    "regexp"
    "os"
    "strconv"
)
var linesToWrite []string = make([]string, 0);
var outputFilePrefix string = "maple"


func processLine(line string, mapleNumber string) {{
    regEx := "{regular_expression}"
    if ok, _:= regexp.MatchString(regEx, line); ok {{
        file, err := os.OpenFile((outputFilePrefix + "_filter" + ".txt"), os.O_APPEND | os.O_WRONLY | os.O_CREATE, 0600);
        if err != nil {{
            panic(err);
        }}
        defer file.Close()
        output := line
        file.Write([]byte(output + "{linefeed}"))
    }}

}}

func main(){{
    fmt.Println("Hello world!");
    argsWithoutProg := os.Args[1:]
    if len(argsWithoutProg) < 3 {{
        fmt.Println("Usage: <exec> <myMapleNumber> <totalMaples> <inputFile>");
        return;
    }}
    fmt.Println(argsWithoutProg);
    myMapleNumber, err := strconv.ParseInt(argsWithoutProg[0], 10, 64)
    if err != nil {{
        fmt.Println(err);
    }}
    outputFilePrefix = outputFilePrefix + strconv.Itoa(int(myMapleNumber))
    totalMaples, err := strconv.ParseInt(argsWithoutProg[1], 10, 64)
    if err != nil {{
        fmt.Println(err);
    }}
    inputFile := argsWithoutProg[2]
    keyFile := "keys" + strconv.Itoa(int(myMapleNumber)) + ".txt"
    fmt.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s{linefeed}", myMapleNumber, totalMaples, inputFile, keyFile);
    fmt.Printf("New bitch{linefeed}");
    file, err := os.Open(inputFile)
    if err != nil {{
        fmt.Println(err)
    }}
    idx := int64(0)
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {{
        line := scanner.Text()
        if idx % totalMaples == myMapleNumber {{
            fmt.Printf("Gonna process this line %d now: %s{linefeed}", idx, line);
            processLine(line, argsWithoutProg[0])
        }}
        idx++;
    }}
    file, err = os.OpenFile(keyFile, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
    if err != nil {{
        panic(err)
    }}
    defer file.Close()
    file.Write([]byte("filter{linefeed}"))
    fmt.Println("Done{linefeed}");
}}
'''
    )
with open(outputFile, "w") as f:
    f.write(code_snippet)
print("Done")

