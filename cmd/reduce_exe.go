package main;

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var wordCounts map[string]int = make(map[string]int)
var outputFilePrefix string = "juice"

func processLine(line string){
	tokens := strings.Split(line, ",")
	wordCounts[tokens[0]]++
}

func main(){
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) < 1{
		fmt.Printf("Usage: ./juice_exe <juice_number> <list of filenames>")
		return
	}
	if len(argsWithoutProg) == 1 {
		return
	}
	fnames := argsWithoutProg[1:]
	for _, fname := range(fnames) {
		file, err := os.Open(fname)
		if err != nil {
			fmt.Println(err)
			continue;
		}
		fileScanner := bufio.NewScanner(file)
		fileScanner.Split(bufio.ScanLines)
		for fileScanner.Scan() {
			processLine(fileScanner.Text());
		}
		file.Close()
	}
	// done processing, now time to output
	outputFname := outputFilePrefix + "_" + argsWithoutProg[0] + ".out"
	outputFile, err := os.Create(outputFname)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer outputFile.Close()
	for key, freq := range(wordCounts){
		fmt.Println("key=%s, value=%d\n", key, freq);
		outputLine := key + ":" + strconv.Itoa(freq) + "\n"
		outputFile.Write([]byte(outputLine))
		fmt.Printf("Wrote key=%s\n", key);
	}
}
