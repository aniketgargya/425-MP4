package main;

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var setOfKeys map[string]int = make(map[string]int);
var outputFilePrefix string = "maple"

func processLine(line string, mapleNumber string){
		tokens := strings.Split(line, ",")
		for _, token := range(tokens){
			setOfKeys[token] = 1
			file, err := os.OpenFile((outputFilePrefix + "_" + token + ".txt"), os.O_APPEND | os.O_WRONLY | os.O_CREATE, 0600)
			if err != nil {
				panic(err)
			}
			defer file.Close()
			log.Printf("Successfully opened %s\n", (outputFilePrefix + "_" + token + ".txt"))
			output := token + "," + strconv.Itoa(1)
			file.Write([]byte(output + "\n"))
			log.Printf("Wrote %s to the file\n", output);
			fmt.Printf("%s\n", (output));
		}
}

func main(){
		argsWithoutProg := os.Args[1:]
		if len(argsWithoutProg) != 3 {
			fmt.Println("Usage: <exec> <myMapleNumber> <totalMaples> <inputFile>");
			return;
		}
		logfile, err := os.OpenFile("testlogfile" + argsWithoutProg[0] + ".log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
		if err != nil {
			fmt.Println(err);
		}
		log.SetOutput(logfile)
		defer logfile.Close()
		fmt.Println(argsWithoutProg);
		myMapleNumber, err := strconv.ParseInt(argsWithoutProg[0], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		outputFilePrefix = outputFilePrefix + strconv.Itoa(int(myMapleNumber))
		totalMaples, err := strconv.ParseInt(argsWithoutProg[1], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		inputFile := argsWithoutProg[2]
		keyFile := "keys" + strconv.Itoa(int(myMapleNumber)) + ".txt"
		fmt.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s\n", myMapleNumber, totalMaples, inputFile, keyFile);
		log.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s\n", myMapleNumber, totalMaples, inputFile, keyFile);
		file, err := os.Open(inputFile)
		if err != nil {
			fmt.Println(err)
		}
		idx := int64(0)
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			if idx % totalMaples == myMapleNumber {
				fmt.Println("Gonna process this line %d now: %s\n", idx, line);
				log.Printf("Gonna process this line %d now: %s\n", idx, line);
				processLine(line, argsWithoutProg[0])
			}
			idx++;
		}
		for key, _ := range(setOfKeys){
			file, err := os.OpenFile(keyFile, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
			defer file.Close()
			if err != nil {
				panic(err)
			}
			log.Printf("Opened the file %s\n", keyFile);
			file.Write([]byte(key + "\n"));
			log.Printf("Wrote %s to %s\n", key, keyFile);
		}
		fmt.Println("Done\n");
}
