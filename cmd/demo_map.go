package main;

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var setOfKeys map[string]int = make(map[string]int);
var outputFilePrefix string = "maple"
var cliArg string

func processLine(line string, mapleNumber string){
    tokens := strings.Split(line, ",")

    if tokens[10] == cliArg {
        setOfKeys["_"] = 1
        file, err := os.OpenFile((outputFilePrefix + "_" + "_" + ".txt"), os.O_APPEND | os.O_WRONLY | os.O_CREATE, 0600)
        if err != nil {
            panic(err)
        }
        defer file.Close()
        log.Printf("Successfully opened %s\n", (outputFilePrefix + "_" + "_" + ".txt"))

        output := "_" + "\t" + tokens[9]
        file.Write([]byte(output + "\n"))
        log.Printf("Wrote %s to the file\n", output);
        fmt.Printf("%s\n", (output));
    } else {
        fmt.Printf("tokens[10]: %v\n", tokens[10])
        fmt.Printf("tokens: %v\n", tokens)
    }
}

func main(){
		argsWithoutProg := os.Args[1:]
		if len(argsWithoutProg) < 3 {
			fmt.Println("Usage: <exec> <myMapleNumber> <totalMaples> <inputFile>");
			return;
		}
        fmt.Printf("os.Args: %v\n", os.Args)
        fmt.Printf("argsWithoutProg: %v\n", argsWithoutProg)
        cliArg = argsWithoutProg[len(argsWithoutProg) - 1]
		logfile, err := os.OpenFile("testlogfile" + argsWithoutProg[0] + ".log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
		if err != nil {
			fmt.Println(err);
		}
		log.SetOutput(logfile)
        log.Printf("os.Args: %v\n", os.Args)
        log.Printf("argsWithoutProg: %v\n", argsWithoutProg)
        log.Printf("cliArg: %v\n", cliArg)
		defer logfile.Close()
		fmt.Println(argsWithoutProg);
		myMapleNumber, err := strconv.ParseInt(argsWithoutProg[0], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		outputFilePrefix = outputFilePrefix + strconv.Itoa(int(myMapleNumber))
		totalMaples, err := strconv.ParseInt(argsWithoutProg[1], 10, 64)
		if err != nil {
			fmt.Println(err);
		}
		inputFile := argsWithoutProg[2]
		keyFile := "keys" + strconv.Itoa(int(myMapleNumber)) + ".txt"
		fmt.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s\n", myMapleNumber, totalMaples, inputFile, keyFile);
		log.Printf("myMapleNumber=%d. totalMaples=%d. inputFile=%s, keyFile=%s\n", myMapleNumber, totalMaples, inputFile, keyFile);
		file, err := os.Open(inputFile)
		if err != nil {
			fmt.Println(err)
		}
		idx := int64(0)
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			if idx % totalMaples == myMapleNumber {
				fmt.Println("Gonna process this line %d now: %s\n", idx, line);
				log.Printf("Gonna process this line %d now: %s\n", idx, line);
				processLine(line, argsWithoutProg[0])
			}
			idx++;
		}
		for key, _ := range(setOfKeys){
			file, err := os.OpenFile(keyFile, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
			defer file.Close()
			if err != nil {
				panic(err)
			}
			log.Printf("Opened the file %s\n", keyFile);
			file.Write([]byte(key + "\n"));
			log.Printf("Wrote %s to %s\n", key, keyFile);
		}
		fmt.Println("Done\n");
}
