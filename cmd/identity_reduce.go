package main;

import (
	"bufio"
	"fmt"
	"os"
)

var outputFilePrefix string = "juice"
var outputFname string;
func processLine(line string){

}

func main(){
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) < 1{
		fmt.Printf("Usage: ./juice_exe <juice_number> <list of filenames>")
		return
	}
	if len(argsWithoutProg) == 1 {
		return
	}
	fnames := argsWithoutProg[1:]
	// done processing, now time to output
	outputFname = outputFilePrefix + "_" + argsWithoutProg[0] + ".out"
	outputFile, err := os.Create(outputFname)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer outputFile.Close()
	for _, fname := range(fnames) {
		file, err := os.Open(fname)
		if err != nil {
			fmt.Println(err)
			continue;
		}
		fileScanner := bufio.NewScanner(file)
		fileScanner.Split(bufio.ScanLines)
		for fileScanner.Scan() {
			outputFile.Write([]byte(fileScanner.Text() + "\n"))
		}
		file.Close()
	}
}
