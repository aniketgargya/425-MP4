package mapreduce;

import (
	"fmt"
	// "strconv"
	//"google.golang.org/grpc"
	"golang.org/x/net/context"
	"golang.org/x/sync/semaphore"
	fs "github.com/mjacob1002/425-MP4/pkg/filesystem"
	mem "github.com/mjacob1002/425-MP4/pkg/membership"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"time"
)

// wrapper around SubmitJuiceJob
func SubmitJuiceJob(juiceExe string, totalJuices int64, sdfsInputPrefix string, sdfsDestName string, deleteInput int64){
	juiceReq := JuiceJobRequest{
		JuiceExe: juiceExe,
		TotalJuices: totalJuices,
		SdfsInputPrefix: sdfsInputPrefix,
		SdfsDestName: sdfsDestName,
		DeleteInput: deleteInput,
	}
	if _, ok := MapReduceStubs["Leader"]; !ok {
		fmt.Println("Shit, I can't access the leader stub\n");
	}
	emptyContext := context.Background()
	_, err := MapReduceStubs["Leader"].SubmitJuiceJob(emptyContext, &juiceReq);
	if err != nil {
		fmt.Printf("Something went wrong in SubmitJuiceJob, line 23 of reduce.go. %s\n", err);
	} else {
		fmt.Printf("Successfully submitted the juice job!\n");
	}
}

func (s* MyMapReduceServer) SubmitJuiceJob(ctx context.Context, in* JuiceJobRequest) (*JuiceJobResponse, error) {
	fmt.Printf("Got a juice job of the following: %s\n", in.String());
	workStruct := WorkStruct {
		JobType: 1,
		ReduceIn: in,
	}
	leaderWorkChannel <- workStruct;
	resp := JuiceJobResponse{}
	resp.Ok = 0
	return &resp, nil
}

func RunJuiceJob(in *JuiceJobRequest){
	fmt.Printf("Got the following juice job: %s\n", in.String());
		jobsRemainingSem = semaphore.NewWeighted(in.TotalJuices)
		for i := int64(0); i < in.TotalJuices; i++ {
			jobsRemainingSem.Acquire(context.TODO(), 1)
		}
		DistributeJuiceWork(in.JuiceExe, in.TotalJuices, in.SdfsInputPrefix, in.SdfsDestName, in.DeleteInput);
		for i := int64(0); i < in.TotalJuices; i++ {
			// One iteration of the loop will finish each time the semaphore value is incremented
			jobsRemainingSem.Acquire(context.TODO(), 1)
		}
}

func DistributeJuiceWork(juiceExe string, totalJuices int64, sdfsInputPrefix string, sdfsDestname string, deleteInput int64){
	idx := 0
	juiceNumber := int64(0)
	NumTasksRemaining = totalJuices
	otherMembers := mem.GetOtherMembers()
	NodeToMapleLock.Lock()
	defer NodeToMapleLock.Unlock()
	for juiceNumber < totalJuices {
		reduceReq := ReduceWorkRequest{
			MyJuice: juiceNumber,
			JuiceExe: juiceExe,
			TotalJuices: totalJuices,
			SdfsPrefix: sdfsInputPrefix,
			SdfsDestName: sdfsDestname,
			DeleteInput: deleteInput,
		};
		workRequest := WorkRequest{
				JobType: 1,
				ReduceRequest: &reduceReq,
				TotalTasks: totalJuices,
		}
		fmt.Printf("Assigning %d to node %s\n", juiceNumber, otherMembers[idx]);
		NodeToMaple[otherMembers[idx]] = append(NodeToMaple[otherMembers[idx]], workRequest);
		// TODO: change MyAssignWork to take in a WorkRequest message
		MyAssignWork(otherMembers[idx], &workRequest);
		fmt.Println("got past assigned work");
		juiceNumber++;
		idx = (idx + 1) % len(otherMembers)
	}
}

func (s* MyMapReduceServer) JuiceDone(ctx context.Context, in* JuiceAckRequest) (*JuiceAckResponse, error){
	NodeToMapleLock.Lock()
	defer NodeToMapleLock.Unlock()
	juiceNumber := in.JuiceNumber;
	machineId := in.MachineId;
	fmt.Println("Got an Ack from juiceNumber=", juiceNumber, " on machineId=", machineId);
	NumTasksRemaining--;
	jobsRemainingSem.Release(1);
	var idxRemove int64 = -1;
	for idx, workReq := range(NodeToMaple[machineId]){
		juiceStruct := workReq.ReduceRequest;
		if juiceStruct.MyJuice == juiceNumber {
			idxRemove = int64(idx);
		}
	}
	if idxRemove == -1 {
		fmt.Printf("For some reason, juice number %d ain't here...\n", juiceNumber);
	} else {
		arr := NodeToMaple[machineId]
		arr = append(arr[:idxRemove], arr[idxRemove + 1:]...)
		NodeToMaple[machineId] = arr
	}
	if NumTasksRemaining == 0 {
		fmt.Println("All the juice tasks have completed!\n");
	}
	resp := JuiceAckResponse{}
	return &resp, nil
}

func JuiceDone(in* JuiceAckRequest){
	emptyContext := context.Background()
	MapReduceStubsMutex.Lock()
	_, err := MapReduceStubs["Leader"].JuiceDone(emptyContext, in);
	if err != nil {
		fmt.Println("On line 123 of reduce wrapper function: ", err);
	}
	MapReduceStubsMutex.Unlock()
}

func TransformNames(prefix string, fnames []string) ([]string){
	res := make([]string, 0)
	for _, fname := range(fnames){
		res = append(res, prefix + "_" + fname)
	}
	return res;
}

func ExecuteJuice(in* ReduceWorkRequest){
	fmt.Printf("Executing the following juice: %s\n", in.String());
	// do work for the reduce job here
	juiceExe := in.JuiceExe;
	myJuice := in.MyJuice;
	fmt.Printf("juiceExe=%s, myJuice=%d\n", juiceExe, myJuice)
	exeLocalName := "localjuice" + strconv.Itoa(int(myJuice)) + juiceExe
	GetFile(exeLocalName, juiceExe);
	filesWithPrefix := fs.GetAllFilesWithPrefix(in.SdfsPrefix);
	sort.Strings(filesWithPrefix)
	if len(filesWithPrefix) <+ int(myJuice) {
		fmt.Printf("Juice task %d won't have files to process", myJuice);
		juiceAckReq := JuiceAckRequest{}
		juiceAckReq.JuiceNumber = in.MyJuice
		juiceAckReq.MachineId = mem.ThisMachineId
		// tell the leader that this reduce task is done
		JuiceDone(&juiceAckReq)
		return;
	}
	filesToProcess := make([]string, 0)
	for idx := int(myJuice); idx < len(filesWithPrefix); idx += int(in.TotalJuices) {
			filesToProcess = append(filesToProcess, filesWithPrefix[idx])
	}
	fmt.Println("Gonna process the following files at juice task ", myJuice, " :", filesToProcess);
	fnamePrefix := "juice" + strconv.Itoa(int(myJuice))
	fnamesToProcess := TransformNames(fnamePrefix, filesToProcess)
	for idx, toProcess := range(fnamesToProcess) {
		GetFile(toProcess, filesToProcess[idx])
	}
	// execute the command
	argsForCommand := make([]string, 0)
	argsForCommand = append(argsForCommand, "run", exeLocalName, strconv.Itoa(int(myJuice)))
	argsForCommand = append(argsForCommand, fnamesToProcess...)
	out, err := exec.Command("go", argsForCommand...).Output()
	if err != nil {
		fmt.Println("There was an error in ExecuteJuice when running command: ", err);
	}
	fmt.Printf("Output from juice task %d: %s", myJuice, out)
	// time to write to the destination file
	outputFile := "juice_" + strconv.Itoa(int(myJuice)) + ".out"
	index := fs.GetFileOwner(in.SdfsDestName)
	fs.Put(fs.MachineStubs[fs.MachineIds[index]], outputFile, in.SdfsDestName, false)
	fmt.Printf("Juice task %d wrote to SDFS file %s\n", myJuice, in.SdfsDestName);
	// clean up all the excess files for keys
	for _, toCleanup := range(fnamesToProcess){
			err := os.Remove(toCleanup)
			if err != nil {
				fmt.Println(err)
			}
	}
	err = os.Remove(outputFile)
	if err != nil {
		fmt.Println("Error removing ", outputFile, ": ", err);
	}
	err = os.Remove(exeLocalName)
	if err != nil {
		fmt.Println("Couldn't clean up exec: ", err);
	}
	fmt.Println("Cleaned up my files.\n");
	time.Sleep(3 * time.Second);
	juiceAckReq := JuiceAckRequest{}
	juiceAckReq.JuiceNumber = in.MyJuice
	juiceAckReq.MachineId = mem.ThisMachineId
	// tell the leader that this reduce task is done
	if in.DeleteInput == 1 {
		for _, file := range(filesToProcess) {
			fs.SafeDelete(file);
		}
	}
	JuiceDone(&juiceAckReq)
}
