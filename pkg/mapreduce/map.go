package mapreduce;

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"os/exec"
	"sync"
	"strconv"
	"google.golang.org/grpc"
	"golang.org/x/net/context"
	"golang.org/x/sync/semaphore"
	"time"

	fs "github.com/mjacob1002/425-MP4/pkg/filesystem"
	mem "github.com/mjacob1002/425-MP4/pkg/membership"
)

// stores the stubs for gRPC
var MapReduceStubs map[string] MapReduceClient = make(map[string]MapReduceClient)
var MapReduceStubsMutex sync.Mutex

// have a mapping of nodes to the maple/juice tasks
var NodeToMapleLock sync.Mutex;
var NodeToMaple map[string] []WorkRequest= make(map[string][]WorkRequest);

// keep track of the number of maples remaining
var NumTasksRemaining int64

// this is who the leader is
var LeaderId string = "fa23-cs425-1101.cs.illinois.edu:7000"; // HARDCODED - remove this later
var LeaderStub MapReduceClient;

// the machine that issued the current request
var RequestingMachineId string

// We could potentially have this on every node, just used only on the leader though
var leaderWorkChannel = make(chan WorkStruct)
var jobsRemainingSem *semaphore.Weighted

type WorkStruct struct {
	JobType int // 0 for maple, 1 for juice
	MapIn *MapleJobRequest
	ReduceIn *JuiceJobRequest
}

func InitializeLeaderStub(){
	InitializeMapReduceConnection("Leader", LeaderId)
}

func InitializeMapReduceConnection(machineId string, serverAddress string) {
	conn, err := grpc.Dial(serverAddress, grpc.WithInsecure());
	if err != nil {
		fmt.Printf(fmt.Errorf("grpc.Dial: %v\n", err).Error());
	}
	client := NewMapReduceClient(conn);
	MapReduceStubsMutex.Lock()
	MapReduceStubs[machineId] = client;
	MapReduceStubsMutex.Unlock()
}

// create a maple work struct
func CreateMapleStruct(mapExe string, totalMaples int64, sdfsPrefix string, sdfsDirectory string, myMaple int64, extraArgs string) (MapWorkRequest){
	mapleStruct := MapWorkRequest{}
	mapleStruct.MapExe = mapExe
	mapleStruct.TotalMaples = totalMaples
	mapleStruct.SdfsPrefix = sdfsPrefix
	mapleStruct.SdfsDirectory = sdfsDirectory
	mapleStruct.MyMaple = myMaple
	mapleStruct.ExtraArgs = extraArgs
	return mapleStruct
}

func DistributeMapleWork(mapExe string, totalMaples int64, sdfsPrefix string, sdfsDirectory string, extraArgs string) {
	idx := 0;
	mapleNumber := int64(0) // the current maple being assigned
	NumTasksRemaining = totalMaples
	otherMembers := mem.GetOtherMembers()
	NodeToMapleLock.Lock()
	defer NodeToMapleLock.Unlock()
	for mapleNumber < totalMaples {
		mapleStruct := CreateMapleStruct(mapExe, totalMaples, sdfsPrefix, sdfsDirectory, mapleNumber, extraArgs);
		// TODO: embed mapleStruct in a WorkRequest message
		workRequest := WorkRequest{}
		workRequest.JobType = 0
		workRequest.MapRequest = &mapleStruct
		fmt.Printf("Assigning %d to node %s\n", mapleNumber, otherMembers[idx]);
		NodeToMaple[otherMembers[idx]] = append(NodeToMaple[otherMembers[idx]], workRequest);
		// TODO: change MyAssignWork to take in a WorkRequest message
		MyAssignWork(otherMembers[idx], &workRequest);
		fmt.Println("got past assigned work");
		mapleNumber++;
		idx = (idx + 1) % len(otherMembers);
	}
}

func ReassignMaples(failedNode string){
	otherMembers := mem.GetOtherMembers() // get the other members
	NodeToMapleLock.Lock()
	defer NodeToMapleLock.Unlock()
	idx := 0
	for _, workReq:= range(NodeToMaple[failedNode]) {
		fmt.Println("Other members in the list: ", otherMembers);
		fmt.Printf("Reassigning %s maple to node %s\n", workReq.String(), otherMembers[idx])
		NodeToMaple[otherMembers[idx]] = append(NodeToMaple[otherMembers[idx]], workReq)
		MyAssignWork(otherMembers[idx], &workReq);
		idx = (idx + 1) % len(otherMembers);
	}
}

// boilerplate GRPC stuff for setting up the server
type MyMapReduceServer struct {
	UnimplementedMapReduceServer
}

func InitializeMapReduceGRPCServer(port string) {
	// create the leader stub
	list, err := net.Listen("tcp", ":" + port);
	if err != nil {
		fmt.Printf(fmt.Errorf("net.Listen: %v\n", err).Error());
		os.Exit(1);
	}
	grpcServer := grpc.NewServer();
	serv := MyMapReduceServer{}
	RegisterMapReduceServer(grpcServer, &serv);
	grpcServer.Serve(list);
}

func MyAssignWork(machineId string, in* WorkRequest){
	emptyCtx := context.Background()
	MapReduceStubsMutex.Lock()
	fmt.Printf("Inside assign work. Assigning work to %s\n", machineId);
	_ , err := MapReduceStubs[machineId].AssignWork(emptyCtx, in);
	MapReduceStubsMutex.Unlock()
	if err != nil {
		fmt.Printf("Error in AssignWork, line 95: %s\n", err);
	}
}

func (s* MyMapReduceServer) AssignWork(ctx context.Context, in* WorkRequest) (*MapWorkResponse, error) {
	if in.JobType == 0 {
		go ExecuteMaple(in.MapRequest);
		// do the map job
	} else {
		// do the reduce job
		// fmt.Printf("This node got the following reduce work task: %s\n", in.String());
		go ExecuteJuice(in.ReduceRequest);
	}
	fmt.Printf("RECEIVED! %s\n", in.String())
	response := MapWorkResponse{}
	return &response, nil;
}

// get the file from SDFS
func GetFile(localFname string, sdfsFname string) {
	fs.SafeGet(sdfsFname, localFname);
}

func ReadKeys(mapleNumber int64) ([]string){
	keys := make([]string, 0)
	fname := "keys" + strconv.Itoa(int(mapleNumber)) + ".txt"
	file, err := os.Open(fname)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		line := scanner.Text()
		keys = append(keys, line)
	}
	if err := scanner.Err(); err != nil {
		fmt.Println("Scanner error:", err)
	}
	fmt.Println("Keys for maple number ", mapleNumber,": ", keys);
	return keys
}

func ExecuteMaple(in* MapWorkRequest) {
	fmt.Println("executing maple of the following:", in.String());
	// TODO: execute the maple here
	exeLocalName := "local" + strconv.Itoa(int(in.MyMaple)) + in.MapExe;
	fmt.Printf("exeLocalName=%s\n", exeLocalName);
	GetFile(exeLocalName, in.MapExe);
	// GetFile(exeLocalName, in.mapleExe); // gets the file
	// mapleFilePrefix := "maple" + strconv.Itoa(in.MyMaple)
	// get all the files from the sdfsDir and bring them down
	// for every file, run the executable ./map_exe filename mapleFilePrefix maple_number
	// NOTE: for now, we are just going to plug in the directory as the file name just to see if this runs properly
	// command := fmt.Sprintf("go run %s %d %d %s", exeLocalName, in.MyMaple, in.TotalMaples, in.SdfsDirectory);
	mapleNumberStr := strconv.Itoa(int(in.MyMaple))
	totalMapleStr := strconv.Itoa(int(in.TotalMaples))
	filesWithPrefix := fs.GetAllFilesWithPrefix(in.SdfsDirectory)
	fmt.Println("Got the following files: ", filesWithPrefix);
	for _, fname := range(filesWithPrefix){
			dataset_name := "local_" + mapleNumberStr + "_" + fname
			GetFile(dataset_name, fname);
			sanitizedArgs := fmt.Sprintf("%v", in.ExtraArgs)
			cmd := fmt.Sprintf("go run %s %s %s %s %s", exeLocalName, mapleNumberStr, totalMapleStr, dataset_name, sanitizedArgs)
			// out, err := exec.Command(cmd).Output();
            out, err := exec.Command("go", "run", exeLocalName, mapleNumberStr, totalMapleStr, dataset_name, sanitizedArgs).Output();
			fmt.Println("Command being run: ", cmd);
			if err != nil {
					fmt.Println("error when running the executable: ",err)
			}
			fmt.Printf("%s\n", out);
	}
	keys := ReadKeys(in.MyMaple)
	for _, key := range(keys){
		// put the files onto sdfs by appending
		localFname := "maple" + strconv.Itoa(int(in.MyMaple)) + "_" + key + ".txt"
		sdfsName := in.SdfsPrefix + "_" + key
		fmt.Printf("localFname=%s, getting put to sdfs as %s\n", localFname, sdfsName);
		fs.SafePut(localFname, sdfsName);
		 err := os.Remove(localFname)
		if err != nil {
			fmt.Println(err)
		}
	}
	// read the keys from keys(maple number).txt to get the first set of intermediary files, of the form "mapleFilePrefix_(key).txt"
	// add said intermediary files to the fileNames array and send back to the leader
	mapleNumber := in.MyMaple
	tempfname := string(mapleNumber) + ".txt"
	fileNames := []string{tempfname}
	mapleAckReq := MapleAckRequest{}
	mapleAckReq.MapleNumber = mapleNumber
	mapleAckReq.FileNames = fileNames
	mapleAckReq.MachineId = mem.ThisMachineId;
	// leader.MapleDone
	time.Sleep(3 * time.Second)
	MapleDone(&mapleAckReq);
	fmt.Println("done executing mapleAck for ", mapleNumber);
	err := os.Remove(exeLocalName)
	if err != nil {
		fmt.Println("225: ", err);
	}
	for _, fname := range(filesWithPrefix){
			dataset_name := "local_" + mapleNumberStr + "_" + fname
			err = os.Remove(dataset_name);
			if err != nil {
				fmt.Println("229: ", err);
			}
	}
	err = os.Remove(("keys" + mapleNumberStr + ".txt"))
	if err != nil {
		fmt.Println("233: ", err);
	}
}

func MapleDone(in* MapleAckRequest) {
	emptyContext := context.Background()
	MapReduceStubsMutex.Lock()
	_, err := MapReduceStubs["Leader"].MapleDone(emptyContext, in);
	if err != nil {
		fmt.Println("On line 133 in MapleDone wrapper function: ", err);
	}
	MapReduceStubsMutex.Unlock()
}

// called by a worker, this function executes on the leader
func (s* MyMapReduceServer) MapleDone(ctx context.Context, in* MapleAckRequest) (*MapleAckResponse, error) {
	NodeToMapleLock.Lock() // reusing the lock for the map - replace this if we need our own mutex for the number of maples remaining
	defer NodeToMapleLock.Unlock()
	mapleNumber := in.MapleNumber;
	filenames := in.FileNames;
	machineId := in.MachineId;
	fmt.Println("Got an Ack from mapleNumber=", mapleNumber, " with filenames=", filenames);
	NumTasksRemaining--;
	jobsRemainingSem.Release(1)
	var idxRemove int64 = -1;
	// remove the maple from the mapping
	for idx, workReq:= range(NodeToMaple[machineId]) {
		mapleStruct := workReq.MapRequest
		if mapleStruct.MyMaple == mapleNumber {
			idxRemove = int64(idx)
		}
	}
	if idxRemove == -1 {
		fmt.Printf("Couldn't find maple number %d for machine %s\n", mapleNumber, machineId);
	} else {
		arr := NodeToMaple[machineId]
		arr = append(arr[:idxRemove], arr[idxRemove + 1:]...)
		NodeToMaple[machineId] = arr
		fmt.Println("New array: ", NodeToMaple[machineId]);
	}
	if NumTasksRemaining == 0 {
		fmt.Println("All the maples are done! Proceed to aggregate the keys!");
		// TODO: aggregate the intermediate files
	}
	resp := MapleAckResponse{}
	return &resp, nil

}

// wrapper around SubmitMapleJob
func SubmitMapleJob(mapleExe string, totalMaples int64, sdfsPrefix string, sdfsDir string, extraArgs string) {
	mapleJobReq := MapleJobRequest{}
	mapleJobReq.MapleExe = mapleExe
	mapleJobReq.TotalMaples = totalMaples
	mapleJobReq.SdfsPrefix = sdfsPrefix
	mapleJobReq.SdfsDir = sdfsDir
	mapleJobReq.MachineId = mem.ThisMachineId;
	mapleJobReq.ExtraArgs = extraArgs;
	emptyContext := context.Background()
	if _, ok := MapReduceStubs["Leader"]; !ok {
		fmt.Println("Shit, I can't access the leader stub\n");
	}
	_, err := MapReduceStubs["Leader"].SubmitMapleJob(emptyContext, &mapleJobReq)
	if err != nil {
		fmt.Println("Error on line 190 of mapreduce: %s\n", err);
	}
}

func RunMapleJob(in* MapleJobRequest){
		jobsRemainingSem = semaphore.NewWeighted(in.TotalMaples)
		for i := int64(0); i < in.TotalMaples; i++ {
			jobsRemainingSem.Acquire(context.TODO(), 1)
		}
		DistributeMapleWork(in.MapleExe, in.TotalMaples, in.SdfsPrefix, in.SdfsDir, in.ExtraArgs);
		for i := int64(0); i < in.TotalMaples; i++ {
			// One iteration of the loop will finish each time the semaphore value is incremented
			jobsRemainingSem.Acquire(context.TODO(), 1)
		}
}

// @Mat please choose a better name for this function
// Start this on a separate thread, or just on the main thread potentially
func LeaderWorkerPoller() {
	for {
		// should block while the channel is empty
		inStruct := <-leaderWorkChannel // maple job request
		if(inStruct.JobType == 0){
			RunMapleJob(inStruct.MapIn);
		} else {
			RunJuiceJob(inStruct.ReduceIn);
		}
	}
}

// invoked by a node, executed on the leader
// just queues the work job
func (s* MyMapReduceServer) SubmitMapleJob(ctx context.Context, in* MapleJobRequest)(*MapleJobResponse, error) {
		fmt.Printf("Got job of the following format: %s\n", in.String());
		fmt.Printf("Please move this to be a workqueue or channel with a function that pull from it, calls DistributeMapleWork, and then fucks off\n");
		fmt.Println("Done distributing the work for this job");
		var work WorkStruct;
		work.JobType = 0 // make it a maple job
		work.MapIn = in; // attach the MapleJobRequest to the work struct
		leaderWorkChannel <- work
		resp := MapleJobResponse{}
		return &resp, nil
}
