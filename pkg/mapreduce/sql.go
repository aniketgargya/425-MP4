package mapreduce;

import (
	"fmt"
	fs "github.com/mjacob1002/425-MP4/pkg/filesystem"
	mem "github.com/mjacob1002/425-MP4/pkg/membership"
	"os/exec"
	"strconv"
)
var filterCount int = 0

func SanitizeRegex(tokens []string) (string){
	finalString := ""
	idx := 0
	for idx < (len(tokens) - 1) {
		finalString = finalString + tokens[idx] + " "
		idx++;
	}
	finalString = finalString + tokens[idx]
	return finalString
}

func HandleFilter(expression string, dataset string){
	GENERATE_MAP_EXECUTABLE_PATH := "filter_generator.py"
	reduceOutput := "filter_" + mem.ThisMachineId + "_" + strconv.Itoa(filterCount) + ".txt" // the reduce output
	sdfsDirectory:= dataset
	sdfsPrefix := "mapFilter" + strconv.Itoa(filterCount)
	mapExecutable := "gen_" + strconv.Itoa(filterCount) + ".go"
	MapReduceStubsMutex.Lock()
	numMaples := len(MapReduceStubs)
	MapReduceStubsMutex.Unlock()
	output, err := exec.Command("python3", GENERATE_MAP_EXECUTABLE_PATH, expression, mapExecutable).Output()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Output from the filter_generator.py %s %s: %s", expression, mapExecutable, output)
	sdfsMapExecutable := "sdfs" + mapExecutable
	fs.SafePut(mapExecutable, sdfsMapExecutable);
	SubmitMapleJob(sdfsMapExecutable, int64(numMaples), sdfsPrefix, sdfsDirectory, ""); // submit the maple job
	// do the reduce job here
	fmt.Println(reduceOutput);
	SubmitJuiceJob("identity_reduce.go", 1, sdfsPrefix, reduceOutput, 0);
    filterCount++
}
