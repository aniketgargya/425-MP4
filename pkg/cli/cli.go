package cli;

import (
    "bufio"
    "fmt"
    "os"
    "regexp"
	"strconv"
	membership "github.com/mjacob1002/425-MP4/pkg/membership"
	fs "github.com/mjacob1002/425-MP4/pkg/filesystem"
	mr "github.com/mjacob1002/425-MP4/pkg/mapreduce"
)

func Multiread(sdfsName string) {
	counter := 0;
	machine_list := []string{}
	for machine, _ := range(membership.MembershipList) {
		if counter >= 6 {
			break;
		}
		machine_list = append(machine_list, machine);
		counter++;
	}
	fmt.Println("Nodes to ping: ", machine_list)
	for _, machine := range(machine_list){
		go fs.InvokeARead(machine,  sdfsName)
	}
}

func ListenToCommands(){
    // Define command regular expressions
	mapleArgsRe := regexp.MustCompile(`^maple (\S+) (\S+) (\S+) (\S+) (.*)\n`)
	mapleRe := regexp.MustCompile(`^maple (\S+) (\S+) (\S+) (\S+)\n`)
	filterRe := regexp.MustCompile(`^SELECT ALL FROM (\S+) WHERE -(.+)-\n`);
	juiceRe := regexp.MustCompile(`^juice (\S+) (\S+) (\S+) (\S+) --delete_input=(\S+)\n`)
    putRe := regexp.MustCompile(`^put (\S+) (\S+)\n$`)
    getRe := regexp.MustCompile(`^get (\S+) (\S+)\n$`)
    deleteRe := regexp.MustCompile(`^delete (\S+)\n$`)
    lsRe := regexp.MustCompile(`^ls (\S+)\n$`)
    storeRe := regexp.MustCompile(`^store\n$`)
	multiReadRe := regexp.MustCompile(`^multiread (\S+)\n$`)
    listMemRe := regexp.MustCompile(`^list_mem\n$`)

    reader := bufio.NewReader(os.Stdin)
    for {
        // Read input
        input, _ := reader.ReadString('\n')

        switch {
		case filterRe.MatchString(input):
				matches := filterRe.FindStringSubmatch(input)
				fmt.Printf("Found filter for %s with regex=<%s>\n", matches[1], matches[2]);
                mr.HandleFilter(matches[2], matches[1])
		case mapleRe.MatchString(input):
				matches:= mapleRe.FindStringSubmatch(input)
				fmt.Println(matches[1:])
				numMaples, err := strconv.ParseInt(matches[2], 10, 64)
				if err != nil {
					fmt.Println(err);
				}
				mr.SubmitMapleJob(matches[1], numMaples, matches[3], matches[4], "")
		case mapleArgsRe.MatchString(input):
				matches:= mapleArgsRe.FindStringSubmatch(input)
				fmt.Println(matches[1:])
				numMaples, err := strconv.ParseInt(matches[2], 10, 64)
				if err != nil {
					fmt.Println(err);
				}
				mr.SubmitMapleJob(matches[1], numMaples, matches[3], matches[4], matches[5])
		case juiceRe.MatchString(input):
				matches := juiceRe.FindStringSubmatch(input)
				fmt.Println(matches[1:])
				numJuices, err := strconv.ParseInt(matches[2], 10, 64)
				if err != nil {
					fmt.Println(err)
				}
				deleteOutput, err := strconv.ParseInt(matches[len(matches) -1], 10, 64);
				if err != nil {
					fmt.Println(err);
				}
				mr.SubmitJuiceJob(matches[1], numJuices, matches[3], matches[4], deleteOutput);
				fmt.Printf("Detected a juice job with %d juices!\n", numJuices);
		case multiReadRe.MatchString(input):
			matches := multiReadRe.FindStringSubmatch(input)
			fmt.Println("Mutliread for file ", matches[1])
			Multiread(matches[1])
        case putRe.MatchString(input):
            matches := putRe.FindStringSubmatch(input)
            if len(matches) == 3 {
                localFilename := matches[1]
                sdfsFilename := matches[2]
                index := fs.GetFileOwner(sdfsFilename)
                fs.Put(fs.MachineStubs[fs.MachineIds[index]], localFilename, sdfsFilename, false)
            }
        case getRe.MatchString(input):
            matches := getRe.FindStringSubmatch(input)
            if len(matches) == 3 {
                sdfsFilename := matches[1]
                localFilename := matches[2]
                index := fs.GetFileOwner(sdfsFilename)
                fs.Get(fs.MachineStubs[fs.MachineIds[index]], sdfsFilename, localFilename)
            }
        case deleteRe.MatchString(input):
            matches := deleteRe.FindStringSubmatch(input)
            if len(matches) == 2 {
                sdfsFilename := matches[1]
                index := fs.GetFileOwner(sdfsFilename)
                fs.Delete(fs.MachineStubs[fs.MachineIds[index]], sdfsFilename, false)
            }
        case lsRe.MatchString(input):
            matches := lsRe.FindStringSubmatch(input)
            if len(matches) == 2 {
                sdfsFilename := matches[1]
                index := fs.GetFileOwner(sdfsFilename)

                for i := 0; i < 4; i++ {
                    idx := (i + index) % len(fs.MachineIds)
                    if i != 0 && idx == index {
                        break
                    }

                    fmt.Printf("%v. %v\n", i + 1, fs.MachineIds[idx])
                }
            }
        case storeRe.MatchString(input):
            for i, file := range fs.Files {
                fmt.Printf("%v. %v\n", i + 1, file)
            }
        case listMemRe.MatchString(input):
            for i, machineId := range fs.MachineIds {
                fmt.Printf("%v. %v\n", i + 1, machineId)
            }
        default:
            fmt.Printf("Could not recoginize command\n")
        }
    }
}

